﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logger
{
    public class Logger: IDisposable
    {
        private string _outputPath;

        private FileStream _fileStream;
        private BinaryWriter _binaryWriter;

        public Logger(string outputFilePath)
        {
            _outputPath = outputFilePath;
        }

        public void Dispose()
        {
            _binaryWriter.Dispose();
            _fileStream.Dispose();
        }

        public void Log(string value)
        {
            if (_fileStream == null)
                OpenStream();

            _binaryWriter.Write(value);
        }

        private void OpenStream()
        {
            _fileStream = new FileStream
                (_outputPath, FileMode.Append, FileAccess.Write, FileShare.Write);

            _binaryWriter = new BinaryWriter(_fileStream);
        }
    }
}
