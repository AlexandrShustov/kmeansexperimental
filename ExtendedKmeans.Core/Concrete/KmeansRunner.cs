﻿using ExtendedKmeans.Core.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtendedKmeans.Core.Concrete
{
    public struct DistanceResult<T>
    {
        public readonly int ClusterId;
        public readonly IClusterizedItem<T> Item;
        public readonly double DistanceValue;

        public DistanceResult(IClusterizedItem<T> item, int clusterId, double distanceValue)
        {
            ClusterId = clusterId;
            Item = item;
            DistanceValue = distanceValue;
        }
    }

    public struct ClusterizationResult<T>
    {
        public readonly IEnumerable<ICluster<T>> Clusters;
        public readonly IEnumerable<IClusterizedItem<T>> Items;

        public ClusterizationResult(IEnumerable<ICluster<T>> clusters, IEnumerable<IClusterizedItem<T>> items)
        {
            Clusters = clusters;
            Items = items;
        }
    }

    public abstract class KmeansRunner<T>
    {
        protected int _maxIterationsCount;
        protected int _requiredClusters;
        
        protected List<ICluster<T>> _clusters;
        protected List<IClusterizedItem<T>> _items;

        public ClusterizationResult<T> Run()
        {
            InitializeStartClusters();

            var iterationNumber = 0;
            for (; ; )
            {
                iterationNumber++;

                AssignItemsToNearestClusters();
                RecalcClustersCentroids();

                if (!_clusters.Select(cluster => cluster.Centroid).Any(centroid => centroid.PositionChanged) || iterationNumber == _maxIterationsCount)
                    break;
            }

            return BuildResult();
        }

        private ClusterizationResult<T> BuildResult()
        {
            return new ClusterizationResult<T>(_clusters, _items);
        }

        protected abstract void InitializeStartClusters();
        
        private void AssignItemsToNearestClusters()
        {
            foreach (var item in _items)
            {
                var distancesToClusters = GetDistancesBetween(item, _clusters);
                var minimalDistance = distancesToClusters.OrderBy(distResult => distResult.DistanceValue).First();

                item.SetCluster(minimalDistance.ClusterId);
            }
        }

        private List<DistanceResult<T>> GetDistancesBetween(IClusterizedItem<T> item, List<ICluster<T>> clusters)
        {
            var distances = new List<DistanceResult<T>>();

            clusters.ForEach(cluster =>
                distances.Add(new DistanceResult<T>(
                    item,
                    cluster.Id,
                    item.GetDistanceFrom(cluster.Centroid))));

            return distances;
        }

        private void RecalcClustersCentroids()
        {
            _clusters.ForEach(cluster =>
                cluster.RecalcPositionWith(_items
                    .Where(item => item.RelatedClusterId == cluster.Id)
                    .Select(item => item.Item)));
        }
    }
}
