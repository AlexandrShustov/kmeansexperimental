﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtendedKmeans.Core.Abstract
{
    public interface IClusterizedItem<T>
    {
        T Item { get; }

        int RelatedClusterId { get; }
        double GetDistanceFrom(ICentroid<T> centroid);
        void SetCluster(int clusterId);
    }
}
