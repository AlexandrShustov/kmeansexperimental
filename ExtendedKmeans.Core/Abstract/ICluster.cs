﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtendedKmeans.Core.Abstract
{
    public interface ICluster<T>
    {
        int Id { get; }
        ICentroid<T> Centroid { get; }
        void RecalcPositionWith(IEnumerable<T> relatedItems);
    }
}
