﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtendedKmeans.Core.Abstract
{
    public interface ICentroid<T>
    {
        bool PositionChanged { get; }
        T GetPosition();
        void SetPosition(T newPosition);
    }
}
