﻿using FuzzyKmeans.Abstract;
using PixelClusterizationImpl.Core;
using PixelFuzzyKmeansRunner.Concrete;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace PixelFuzzyKmeansRunner
{
    public class FuzzyKmeansForPixelsRunner : FuzzyKmeans.FuzzyKmeansRunner<Pixel>
    {
        private Logger.Logger _logger;

        private int _imageHeight;
        private int _imageWidth;

        public FuzzyKmeansForPixelsRunner(Bitmap image, int clusterCount, double fuzzyCoef, double epsilon, int maxIterations)
        {
            _logger = new Logger.Logger(@"S:\diploma\log.txt");

            if (fuzzyCoef <= 1)
                throw new ArgumentException("Fuzzy coef should be greater than 1.");

            _imageHeight = image.Height;
            _imageWidth = image.Width;

            _clustersCount = clusterCount;
            _fuzzyCoef = fuzzyCoef;
            _epsilon = epsilon;
            _maxIterationsCount = maxIterations;

            _items = PixelKmeansRunner.ConvertToClusterized(image).ToList();
            _membershipMatrixes = new List<MembershipMatrix<Pixel>>();

            _clusters = new List<IFuzzyCluster<Pixel>>();
        }

        protected override void InitializeMembershipMatrix()
        {
            var random = new Random();

            foreach(var item in _items)
            {
                _membershipMatrixes.Add(new MembershipMatrix<Pixel>(item));
            }

            foreach (var matrix in _membershipMatrixes)
            {
                foreach (var cluster in _clusters)
                {
                    matrix.ClusterIdByMembershipValue.Add(cluster.Id, random.NextDouble());
                }

                //LogClusterIdByMembership("InitMembershipsMatrix", matrix.ClusterIdByMembershipValue, _logger);
            }
        }

        public static void LogClusterIdByMembership(string title, Dictionary<int, double> matrix, Logger.Logger logger)
        {
            var builder = new StringBuilder();

            builder.Append(title + "\r\n");

            foreach(var kvp in matrix)
            {
                builder.Append($"{kvp.Key} :: {kvp.Value}" + "\r\n");
            }

            logger.Log(builder.ToString());
        }

        protected override void InitializeStartClusters()
        {
            for (int i = 0; i < _clustersCount; i++)
            {
                var cluster = new FuzzyPixelCluster(i);

                //var exceptItems = _clusters.Where(c => c.Centroid.PositionChanged);

                //cluster.Centroid.SetPosition(PixelUtils.GetRandomElementFrom(_items.Select(it => it.Item),
                //    exceptItems.Select(c => c.Centroid.GetPosition())));

                cluster.Centroid.SetPosition(PixelUtils.GetRandom());

                _clusters.Add(cluster);
            }
        }
    }
}
