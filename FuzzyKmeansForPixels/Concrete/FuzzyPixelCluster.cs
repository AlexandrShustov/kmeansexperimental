﻿using ExtendedKmeans.Core.Abstract;
using FuzzyKmeans.Abstract;
using PixelClusterizationImpl.Core;
using PixelClusterizationImpl.Core.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PixelFuzzyKmeansRunner.Concrete
{
    public class FuzzyPixelCluster : IFuzzyCluster<Pixel>
    {
        private PixelCentroid _centroid;

        public int Id { get; }

        public ICentroid<Pixel> Centroid => _centroid;

        public FuzzyPixelCluster(int clusterId)
        {
            Id = clusterId;
            _centroid = new PixelCentroid();
        }

        public void RecalcPositionWith(IEnumerable<MembershipMatrix<Pixel>> membershipMatrixes, double tolerance)
        {
            var totalDenominator = 0d;

            var totalR = 0d;
            var totalG = 0d;
            var totalB = 0d;

            foreach (var matrix in membershipMatrixes)
            {
                var denominator = Math.Pow(matrix.ClusterIdByMembershipValue[Id], tolerance);

                totalR += denominator * matrix.Item.Item.Color.R;
                totalG += denominator * matrix.Item.Item.Color.G;
                totalB += denominator * matrix.Item.Item.Color.B;

                totalDenominator += denominator;
            }

            var r = totalR / totalDenominator;
            var g = totalG / totalDenominator;
            var b = totalB / totalDenominator;

            //r = double.IsNaN(r) ? 0 : r;
            //g = double.IsNaN(g) ? 0 : g;
            //b = double.IsNaN(b) ? 0 : b;

            var newCenter = new Pixel((int)r, (int)g, (int)b, 0, 0);
            _centroid.SetPosition(newCenter);
        }
    }
}
