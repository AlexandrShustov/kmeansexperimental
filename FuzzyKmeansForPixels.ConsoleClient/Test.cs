﻿using ImageFileLoader;
using PixelClusterizationImpl.Core;
using PixelFuzzyKmeansRunner;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ImageFileLoader.BitmapSaver;

namespace FuzzyKmeansForPixels.ConsoleClient
{
    public class Test
    {
        public static void Main()
        {
            var path = @"S:\diploma\img.bmp";
            var imageLoader = new BitmapLoader();
            var clustersCount = 3;

            var bitmap = imageLoader.LoadFrom(path);
            var runner = new FuzzyKmeansForPixelsRunner(bitmap, clustersCount, 1.5d, 0.1d, 550);
            var result = runner.Run();

            SaveResultAsBitmapsTo(@"S:\diploma\results\", result);
        }

        private static void SaveResultAsBitmapsTo(string filePath,
            FuzzyKmeansForPixelsRunner.FuzzyClusterizationResult result)
        {
            var bitmapSaver = new BitmapSaver();

            foreach(var cluster in result.Clusters)
            {
                var pointsOfCluster = result.MembershipMatrixes
                    .Where(matrix => matrix.ClosestClusterId == cluster.Id);

                if (!pointsOfCluster.Any())
                    continue;

                var pointsToDraw = CreateBitmapPointsFrom(pointsOfCluster.Select(p => p.Item.Item));

                bitmapSaver.CreateAndSaveBitmapFrom(pointsToDraw, filePath + "cluster_" + cluster.Id);
            }
        }

        private static IEnumerable<BitmapPoint> CreateBitmapPointsFrom(IEnumerable<Pixel> items)
        {
            var result = new List<BitmapPoint>();

            foreach (var item in items)
            {
                result.Add(new BitmapPoint(item.X, item.Y, item.Color));
            }

            return result;
        }
    }
}
