﻿using ExtendedKmeans.Core.Concrete;
using ImageFileLoader;
using PixelClusterizationImpl.Core;
using System.Collections.Generic;
using System.Linq;
using static ImageFileLoader.BitmapSaver;

namespace KmeansClient
{
    public class Test
    {
        public static void Main()
        {
            var path = @"S:\diploma\img.bmp";
            var imageLoader = new BitmapLoader();
            var clustersCount = 9;
            
            var bitmap = imageLoader.LoadFrom(path);
            var runner = new PixelKmeansRunner(clustersCount, bitmap, 150);
            var result = runner.Run();

            SaveResultAsBitmapsTo(@"S:\diploma\results\", result);
        }

        private static void SaveResultAsBitmapsTo(string filePath, ClusterizationResult<Pixel> result)
        {
            var bitmapSaver = new BitmapSaver();
            
            foreach(var cluster in result.Clusters)
            {
                var pointsOfCluster = result.Items.Where(item => item.RelatedClusterId == cluster.Id);

                if (!pointsOfCluster.Any())
                    continue;

                var points = CreateBitmapPointsFrom(pointsOfCluster.Select(point => point.Item));

                bitmapSaver.CreateAndSaveBitmapFrom(points, filePath + "cluster_" + cluster.Id);
            }
        }

        private static IEnumerable<BitmapPoint> CreateBitmapPointsFrom(IEnumerable<Pixel> items)
        {
            var result = new List<BitmapPoint>();

            foreach(var item in items)
            {
                result.Add(new BitmapPoint(item.X, item.Y, item.Color));
            }

            return result;
        }
    }
}
