﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PixelClusterizationImpl.Core
{
    public class Pixel
    {
        public readonly int X;
        public readonly int Y;

        public readonly Color Color;

        public Pixel(int r, int g, int b, int x, int y)
        {
            X = x;
            Y = y;

            Color = Color.FromArgb(r, g, b);
        }
    }
}
