﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PixelClusterizationImpl.Core
{
    public static class PixelUtils
    {
        public static Pixel GetRandomElementFrom(IEnumerable<Pixel> sequence, IEnumerable<Pixel> exceptSequence)
        {
            var random = new Random();

            var exceptSeqColors = exceptSequence.Select(item => item.Color);
            var targetSequence = sequence
                .Where(item => !exceptSeqColors.Contains(item.Color));

            var index = random.Next(0, targetSequence.Count());

            return targetSequence.ElementAt(index);
        }

        public static Pixel GetRandom()
        {
            var random = new Random();

            var r = random.Next(20, 235);
            var g = random.Next(20, 235);
            var b = random.Next(20, 235);

            return new Pixel(r, g, b, 0, 0);
        }
    }
}
