﻿using ExtendedKmeans.Core.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PixelClusterizationImpl.Core.Concrete
{
    class PixelCluster : ICluster<Pixel>
    {
        public PixelCluster(int id)
        {
            Id = id;
        }

        private PixelCentroid _centroid = new PixelCentroid();

        public int Id { get; private set; }
        public ICentroid<Pixel> Centroid => _centroid;

        public void RecalcPositionWith(IEnumerable<Pixel> relatedItems)
        {
            if (!relatedItems.Any())
                return;

            var averageR = relatedItems.Sum(item => item.Color.R) / relatedItems.Count();
            var averageG = relatedItems.Sum(item => item.Color.G) / relatedItems.Count();
            var averageB = relatedItems.Sum(item => item.Color.B) / relatedItems.Count();

            var averageX = relatedItems.Sum(item => item.X) / relatedItems.Count();
            var averageY = relatedItems.Sum(item => item.Y) / relatedItems.Count();

            _centroid.SetPosition(new Pixel(averageR, averageG, averageB, averageX, averageY));
        }
    }
}
