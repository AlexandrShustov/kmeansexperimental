﻿using ExtendedKmeans.Core.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PixelClusterizationImpl.Core.Concrete
{
    public class PixelCentroid : ICentroid<Pixel>
    {
        private Pixel _currentPosition = new Pixel(0, 0, 0, 0, 0);

        public bool PositionChanged { get; private set; }

        public Pixel GetPosition()
        {
            return _currentPosition;
        }

        public void SetPosition(Pixel newPosition)
        {
            if(_currentPosition.Color == newPosition.Color)
            {
                PositionChanged = false;
                return;
            }

            _currentPosition = newPosition;
            PositionChanged = true;
        }
    }
}
