﻿using ExtendedKmeans.Core.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PixelClusterizationImpl.Core.Concrete
{
    public class PixelClusterizedItem : IClusterizedItem<Pixel>
    {
        public Pixel Item { get; }

        public int RelatedClusterId { get; private set; }

        public PixelClusterizedItem(Pixel pixel)
        {
            Item = pixel;
        }

        public double GetDistanceFrom(ICentroid<Pixel> centroid)
        {
            var centroidPosition = centroid.GetPosition();

            return Math.Sqrt(
                Math.Pow((Item.Color.R - centroidPosition.Color.R), 2) +
                Math.Pow((Item.Color.G - centroidPosition.Color.G), 2) +
                Math.Pow((Item.Color.B - centroidPosition.Color.B), 2));
        }

        public void SetCluster(int clusterId)
        {
            RelatedClusterId = clusterId;
        }
    }
}
