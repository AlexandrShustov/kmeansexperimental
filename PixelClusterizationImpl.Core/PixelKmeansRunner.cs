﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExtendedKmeans.Core.Abstract;
using PixelClusterizationImpl.Core.Concrete;

namespace PixelClusterizationImpl.Core
{
    public class PixelKmeansRunner : ExtendedKmeans.Core.Concrete.KmeansRunner<Pixel>
    {
        private int _imageHeight;
        private int _imageWidth;

        public PixelKmeansRunner(int clustersCount, Bitmap image, int maxIterationsCount = 0)
        {
            _imageHeight = image.Height;
            _imageWidth = image.Width;

            _requiredClusters = clustersCount;
            _items = ConvertToClusterized(image).ToList();
            _maxIterationsCount = maxIterationsCount;

            _clusters = new List<ICluster<Pixel>>();
        }

        public static IEnumerable<IClusterizedItem<Pixel>> ConvertToClusterized(Bitmap image)
        {
            var clusterizedItems = new List<PixelClusterizedItem>();

            for (int i = 0; i < image.Width; i++)
            {
                for (int j = 0; j < image.Height; j++)
                {
                    var pixel = image.GetPixel(i, j);

                    if (pixel.R == 255 && pixel.G == 255 && pixel.B == 255)
                        continue; //skip white space

                    clusterizedItems.Add(new PixelClusterizedItem(new Pixel(pixel.R, pixel.G, pixel.B, i, j)));
                }
            }

            return clusterizedItems;
        }

        protected override void InitializeStartClusters()
        {
            for (int i = 0; i < _requiredClusters; i++)
                _clusters.Add(new PixelCluster(i));

            foreach(var cluster in _clusters)
            {
                var randPosition = PixelUtils.GetRandomElementFrom(
                    _items
                        .Select(item => item.Item),
                    _clusters
                        .Where(c => c.Centroid.PositionChanged)
                        .Select(c => c.Centroid.GetPosition()));

                cluster.Centroid.SetPosition(randPosition);
            }
        }
    }
}
