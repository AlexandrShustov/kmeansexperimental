﻿using ExtendedKmeans.Core.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuzzyKmeans.Abstract
{
    public class MembershipMatrix<T>
    {
        public IClusterizedItem<T> Item { get; private set; }
        public Dictionary<int, double> ClusterIdByMembershipValue { get; }
        public Dictionary<int, double> ClusterIdByDistance { get; }

        public int ClosestClusterId => GetClosestCluster();

        private int GetClosestCluster()
        {
            var order = ClusterIdByMembershipValue.OrderBy(kvp => kvp.Value);

            return order.Last().Key;
        }

        public MembershipMatrix(IClusterizedItem<T> item)
        {
            Item = item;
            ClusterIdByMembershipValue = new Dictionary<int, double>();
            ClusterIdByDistance = new Dictionary<int, double>();
        }

        public double GetMembershipValueFor(int clusterId)
        {
            return ClusterIdByMembershipValue[clusterId];
        }

        public double GetDistanceFrom(int clusterId)
        {
            return ClusterIdByDistance[clusterId];
        }

        public void SetMembershipValueFor(int clusterId, double value)
        {
            if (double.IsNaN(value) || double.IsInfinity(value) || value == 0)
            {
                var t = 0;
            }

            if(ClusterIdByMembershipValue.ContainsKey(clusterId))
            {
                ClusterIdByMembershipValue[clusterId] = value;
            }
        }

        public void SetDistanceFor(int clusterId, double distance)
        {
            if (!ClusterIdByDistance.ContainsKey(clusterId))
            {
                ClusterIdByDistance.Add(clusterId, distance);
                return;
            }

            ClusterIdByDistance[clusterId] = distance;
        }

        public void ClearMembershipMatrix()
        {
            ClusterIdByMembershipValue.Clear();
        }

        public void Normalize()
        {
            var total = ClusterIdByMembershipValue.Values.Sum();
            var normalizedValues = new Dictionary<int, double>();

            foreach(var kvp in ClusterIdByMembershipValue)
            {
                normalizedValues.Add(kvp.Key, kvp.Value / total);
            }

            foreach (var value in normalizedValues)
                SetMembershipValueFor(value.Key, value.Value);
        }
    }
}
