﻿using ExtendedKmeans.Core.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuzzyKmeans.Abstract
{
    public interface IFuzzyCluster<T>
    {
        int Id { get; }

        ICentroid<T> Centroid { get; }

        void RecalcPositionWith(IEnumerable<MembershipMatrix<T>> relationValues, double tolerance);
    }
}
