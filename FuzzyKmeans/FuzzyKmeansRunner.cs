﻿using ExtendedKmeans.Core.Abstract;
using ExtendedKmeans.Core.Concrete;
using FuzzyKmeans.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuzzyKmeans
{
    public abstract class FuzzyKmeansRunner<T>
    {
        public struct FuzzyClusterizationResult
        {
            public IEnumerable<MembershipMatrix<T>> MembershipMatrixes { get; }
            public IEnumerable<IFuzzyCluster<T>> Clusters { get; }

            public FuzzyClusterizationResult(IEnumerable<MembershipMatrix<T>> matrixes,
                                             IEnumerable<IFuzzyCluster<T>> clusters)
            {
                MembershipMatrixes = matrixes;
                Clusters = clusters;
            }
        }

        protected int _maxIterationsCount;

        protected List<IFuzzyCluster<T>> _clusters;
        protected List<IClusterizedItem<T>> _items;
        protected List<MembershipMatrix<T>> _membershipMatrixes;

        protected int _clustersCount;
        protected double _fuzzyCoef;
        protected double _epsilon;

        public FuzzyClusterizationResult Run()
        {
            InitializeStartClusters();
            InitializeMembershipMatrix();

            double previousDecision = 0;
            double currentDecision = 0;

            var iterationsCount = 0;
            for( ; ; )
            {
                iterationsCount++;

                RecalcClustersCentroidsPosition(_fuzzyCoef);
                RecalcMembershipValues(_fuzzyCoef);

                currentDecision = CalcDecision();

                if(Math.Abs(currentDecision - previousDecision) < _epsilon
                    || iterationsCount >= _maxIterationsCount)
                {
                    return new FuzzyClusterizationResult(_membershipMatrixes, _clusters);
                }

                previousDecision = currentDecision;
            }
        }

        private double CalcDecision()
        {
            double decision = 0.0d;

            foreach(var matrix in _membershipMatrixes)
            {
                foreach(var cluster in _clusters)
                {
                    var distanceToCluster = matrix.GetDistanceFrom(cluster.Id);
                    var membershipValue = matrix.GetMembershipValueFor(cluster.Id);

                    decision += distanceToCluster * membershipValue;
                }
            }

            return decision;
        }

        private void RecalcMembershipValues(double tolerance)
        {
            foreach(var membershipMatrix in _membershipMatrixes)
            {
                var item = membershipMatrix.Item;

                var newMembershipMatrix = new Dictionary<int, double>();

                foreach(var kvp in membershipMatrix.ClusterIdByMembershipValue)
                {
                    var cluster = _clusters.First(c => c.Id == kvp.Key);
                    var distance = item.GetDistanceFrom(cluster.Centroid);
                    membershipMatrix.SetDistanceFor(cluster.Id, distance);

                    var membreshipValue = CalcMembershipValueFor(distance, tolerance);
                    newMembershipMatrix.Add(cluster.Id, membreshipValue);
                }

                membershipMatrix.ClearMembershipMatrix();
                newMembershipMatrix.Normalize();

                foreach (var kvp in newMembershipMatrix)
                    membershipMatrix.ClusterIdByMembershipValue.Add(kvp.Key, kvp.Value);
            }
        }

        private double CalcMembershipValueFor(double distance, double tolerance)
        {
            var result = Math.Pow(1 /distance, 2 / (tolerance - 1));

            return distance == 0 ? 0.0001d : result;
        }

        private void RecalcClustersCentroidsPosition(double tolerance)
        {
            _clusters.ForEach(cluster =>
            {
                cluster.RecalcPositionWith(_membershipMatrixes, tolerance);
            });
        }

        protected abstract void InitializeStartClusters();

        protected abstract void InitializeMembershipMatrix();
    }
}
