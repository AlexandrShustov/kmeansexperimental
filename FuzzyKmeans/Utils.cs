﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuzzyKmeans
{
    public static class Utils
    {
        public static void Normalize(this Dictionary<int, double> self)
        {
            var sum = self.Sum(kvp => kvp.Value);
            var newDict = new Dictionary<int, double>();

            foreach(var kvp in self)
            {
                var normalized = kvp.Value / sum;

                newDict.Add(kvp.Key, normalized);
            }

            self.Clear();

            foreach (var kvp in newDict)
                self.Add(kvp.Key, kvp.Value);
        }
    }
}
