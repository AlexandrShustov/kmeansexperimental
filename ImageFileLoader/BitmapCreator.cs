﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageFileLoader
{
    public class BitmapSaver
    {
        public class BitmapPoint
        {
            public readonly int X;
            public readonly int Y;
            public readonly Color Color;

            public BitmapPoint(int x, int y, Color color)
            {
                X = x;
                Y = y;
                Color = color;
            }
        }

        public void CreateAndSaveBitmapFrom(IEnumerable<BitmapPoint> pixels, string filePath)
        {
            var bitmap = CreateBitmapFrom(pixels);

            bitmap.Save(filePath + ".bmp");
        }

        private Bitmap CreateBitmapFrom(IEnumerable<BitmapPoint> pixels)
        {
            var maxHeight = pixels.Max(pixel => pixel.Y);
            var maxWidth = pixels.Max(pixel => pixel.X);

            var bitmap = new Bitmap(maxWidth + 1, maxHeight + 1);

            foreach(var pixel in pixels)
            {
                bitmap.SetPixel(pixel.X, pixel.Y, pixel.Color);
            }

            return bitmap;
        }
    }
}
